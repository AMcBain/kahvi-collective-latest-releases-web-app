package controllers;

import java.io.ByteArrayInputStream;
import java.util.List;

import play.cache.Cache;
import play.mvc.Controller;

import models.Playlist;

public class Application extends Controller
{
    public static final String DATA_KEY = "data key";

    public static void index ()
    {
        List<Playlist> releases = Cache.get(DATA_KEY, List.class);
        render(releases);
    }

    public static void image (int index)
    {
        List<Playlist> list = (List<Playlist>)Cache.get(DATA_KEY, List.class);
        byte[] image = (index - 1 < list.size() ? list.get(index - 1).image : null);

        if (image != null)
        {
            response.contentType = "image/png";
            renderBinary(new ByteArrayInputStream(image));
        }
        notFound();
    }
}
