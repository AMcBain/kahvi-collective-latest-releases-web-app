import play.jobs.Job;
import play.jobs.OnApplicationStart;

@OnApplicationStart
public class StartupJob extends Job
{
    public void doJob ()
    {
        RecurringJob.doJobNow();
    }
}
