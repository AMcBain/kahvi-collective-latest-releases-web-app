package models;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("track")
public class Track
{
    public String creator;
    public String title;
    public String album;
    public String image;
    public String location;
    public String info;
}
