package models;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("rss")
public class Rss
{
    public Channel channel;
}
