package models;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.List;

@XStreamAlias("playlist")
public class Playlist
{
    public String title;
    public byte[] image;
    public String info;
    public String description;
    public List<Track> trackList;
}
