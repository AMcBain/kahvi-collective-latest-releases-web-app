package models;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("channel")
public class Channel
{
    public String title;
    @XStreamImplicit
    public List<String> link;
    public String description;
    public String language;
    public Image image;
    @XStreamImplicit
    public List<Item> item;
}
