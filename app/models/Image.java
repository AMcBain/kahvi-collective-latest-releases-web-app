package models;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("image")
public class Image
{
    public String link;
    public String url;
    public String title;
}
