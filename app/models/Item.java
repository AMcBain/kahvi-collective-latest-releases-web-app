package models;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("item")
public class Item
{
    public String link;
    public String guid;
    public String title;
    public String description;
}
