import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.InputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import play.Logger;
import play.Play;
import play.cache.Cache;
import play.classloading.ApplicationClasses;
import play.classloading.ApplicationClasses.ApplicationClass;
import play.jobs.Every;
import play.jobs.Job;

import controllers.Application;

import models.Item;
import models.Playlist;
import models.Rss;
import models.Track;

@Every("1d")
public class RecurringJob extends Job
{
    private static XStream xstream;
    static
    {
        xstream = new XStream(new StaxDriver());

        for (ApplicationClass clazz : Play.classes.getAnnotatedClasses(XStreamAlias.class))
        {
            xstream.processAnnotations(clazz.javaClass);
        }
    }

    public void doJob ()
    {
        doJobNow();
    }

    public static void doJobNow ()
    {
        InputStreamReader is = null;
        try
        {
            URL url = new URL("http://kahvi.org/rss.php");
            is = new InputStreamReader(url.openStream(), "UTF-8");
            List<Playlist> lists = new ArrayList<Playlist>();

            for (Item item : ((Rss)xstream.fromXML(is)).channel.item)
            {
                InputStreamReader plis = null;
                try
                {
                    url = new URL(item.guid.replace("releases.php", "playlist.php"));
                    plis = new InputStreamReader(url.openStream(), "UTF-8");

                    Playlist playlist = (Playlist)xstream.fromXML(plis);
                    playlist.title = item.title;
                    playlist.description = item.description.substring(1, item.description.length() - 1);

                    InputStream acis = null;
                    try
                    {
                        String bits = item.title.substring(item.title.indexOf("/") + 1).trim().split(" ")[0];

                        // TODO handle other naming conventions. Some albums use the last word and others combine
                        // all the words together without spaces. One even uses only the first five letters.
                        url = new URL("http://www.kahvi.org/images/albums/" + bits.toLowerCase() + "_lrg.jpg");
                        acis = url.openStream();

                        BufferedImage image = ImageIO.read(acis);
                        int width = 216;
                        int height = width * image.getHeight() / image.getWidth();
                        BufferedImage thumbnail = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

                        Graphics2D graphics = thumbnail.createGraphics();
                        graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                        graphics.drawImage(image, 0, 0, width, height, null);
                        graphics.dispose();

                        try
                        {
                            ByteArrayOutputStream imos = new ByteArrayOutputStream();
                            ImageIO.write(thumbnail, "png", imos);
                            playlist.image = imos.toByteArray();
                        }
                        catch (IOException e)
                        {
                            Logger.error(e, "Failed to resize album cover for guid %s", item.guid);
                        }
                    }
                    catch (IOException e)
                    {
                        Logger.error(e, "Failed to fetch album cover for guid %s", item.guid);
                    }
                    finally
                    {
                        close(acis, "album cover", item.guid);
                    }

                    lists.add(playlist);
                }
                catch (IOException e)
                {
                    Logger.error(e, "Failed to fetch and parse playlist: guid %s", item.guid);
                }
                finally
                {
                    close(plis, "playlist", item.guid);
                }
            }
            Cache.add(Application.DATA_KEY, lists, "36h");
        }
        catch (Exception e)
        {
            Logger.error(e, "Failed to fetch and parse RSS feed.");
        }
        finally
        {
            close(is, "RSS", "");
        }
    }

    private static void close (Closeable is, String type, String subtype)
    {
        if (is != null)
        {
            try
            {
                is.close();
            }
            catch (IOException e)
            {
                Logger.error(e, "Failed to close stream: %s %s", type, subtype);
            }
        }
    }
}
