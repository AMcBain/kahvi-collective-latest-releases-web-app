This is an application using the Play framework 1.3.1

**Setup**

Anything starting with "run" needs to be done from a command line in the
application's directory.

1. run "play secret"

     _this generates a new secret key for the application. by default, the
     application does not ship with one._

2. run "play dependencies"

     _This connects to the internet to download all the required dependencies._
