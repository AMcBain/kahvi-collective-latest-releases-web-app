"use strict";

window.addEventListener("load", function ()
{
    var timeout, players = [];

    function snap ()
    {
        clearTimeout(timeout);

        timeout = setTimeout(function ()
        {
            var offset, index;

            offset = (window.pageXOffset !== undefined ? window.pageXOffset : window.scrollX);
            index = Math.round(offset / window.innerWidth);

            window.scrollTo(Math.min(document.body.scrollWidth - window.innerWidth,
                    index * window.innerWidth + (index - 1) * 2) + 2, 0);
        }, 250);
    }

    window.addEventListener("scroll", snap);
    window.addEventListener("resize", snap);
    window.addEventListener("keydown", function (event)
    {
        if (event.keyCode === 9)
        {
            snap();
        }
    });
    snap();

    function duration (value)
    {
        var minutes, seconds;

        minutes = Math.floor(value / 60);
        seconds = Math.floor(value - minutes * 60);

        return (minutes < 10 ? "0" : "") + minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
    }

    function Player (parent)
    {
        var progress, rect, tracks, audio, changing, button, play, index = 0;

        progress = parent.querySelector("progress");
        progress.addEventListener("mousedown", function (event)
        {
            var mousemove, mouseup;

            if (audio.src)
            {
                mousemove = function (event)
                {
                    event.preventDefault();
                    progress.value = (event.clientX - rect.left) / rect.width * audio.duration;
                };
                window.addEventListener("mousemove", mousemove);

                mouseup = function (event)
                {
                    event.preventDefault();

                    if (changing)
                    {
                        changing = false;
                        audio.currentTime = (event.clientX - rect.left) / rect.width * audio.duration;
                    }
                    window.removeEventListener("mousemove", mousemove);
                    window.removeEventListener("mouseup", mouseup);
                };
                window.addEventListener("mouseup", mouseup);

                rect = progress.getBoundingClientRect();
                mousemove(event);
                changing = true;
            }
        });

        tracks = parent.querySelectorAll("li");
        parent.addEventListener("click", function (event)
        {
            if ("index" in event.target.dataset)
            {
                players.forEach(function (player)
                {
                    player.pause();
                });

                tracks[index].classList.remove("active");
                index = event.target.dataset.index;

                parent.classList.add("loading");
                audio.src = tracks[index].dataset.href;
                tracks[index].classList.add("active");
                play = true;
            }
        });

        audio = new Audio();
        audio.addEventListener("pause", function ()
        {
            button.title = "play";
            button.textContent = "play";
            button.classList.add("paused");
        });
        audio.addEventListener("play", function ()
        {
            button.title = "pause";
            button.textContent = "pause";
            button.classList.remove("paused");
            parent.classList.remove("loading");
        });
        audio.addEventListener("durationchange", function ()
        {
            progress.max = this.duration;
            progress.textContent = "00:00";
        });
        audio.addEventListener("timeupdate", function ()
        {
            if (!changing)
            {
                progress.value = this.currentTime;
                progress.textContent = duration(this.currentTime);
            }
        });
        audio.addEventListener("canplaythrough", function ()
        {
            if (play)
            {
                audio.play();
                play = false;
            }
        });
        audio.addEventListener("ended", function ()
        {
            tracks[index].classList.remove("active");

            if (++index < tracks.length)
            {
                parent.classList.add("loading");
                audio.src = tracks[index].dataset.href;
                tracks[index].classList.add("active");
                play = true;
            }
            else
            {
                index = 0;
            }
        });

        button = parent.querySelector(".button");
        button.addEventListener("click", function ()
        {
            if (audio.paused)
            {
                players.forEach(function (player)
                {
                    player.pause();
                });

                // If we're waiting for playback, hitting the button twice would cause this to force
                // playing even if the buffer doesn't have enough to play without later buffering.
                if (audio.src)
                {
                    audio.play();
                }
                else
                {
                    parent.classList.add("loading");
                    audio.src = tracks[index].dataset.href;
                    tracks[index].classList.add("active");
                    play = true;
                }
            }
            else
            {
                audio.pause();
            }
        });

        this.pause = function ()
        {
            audio.pause();
        };
    }

    Array.prototype.forEach.call(document.querySelectorAll(".player"), function (player)
    {
        players.push(new Player(player.parentNode));
    });
});
